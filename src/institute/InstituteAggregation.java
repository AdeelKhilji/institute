/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Institute Aggregation
 * @author Adeel Khilji
 */
public class InstituteAggregation 
{
    /**
     * main() - main method with one argument
     * @param args String
     */
    public static void main(String[] args)
    {
        List<Student> studentsSDNE = new ArrayList<Student>();//Creating list of students for SDNE
        Student student = new Student(996689685,"Shawn Parker");//Creating an object of student
        studentsSDNE.add(student);//adding the initial object of the student to the list
        student = new Student(444622611,"Jane Wayne");//Creating new object of the student
        studentsSDNE.add(student);//adding student to the list
        student = new Student(555112411,"Kyle Jennings");//Creating new object of the student
        studentsSDNE.add(student);//adding student to the list
        student = new Student(444622611,"Jake Snake");//Creating new object of the student
        studentsSDNE.add(student);//adding student to the list
        
        List<Student> studentsArts = new ArrayList<Student>();//Creating list of students for Arts
        student = new Student(555222333,"Alex Xing");//Creating new object of the student
        studentsArts.add(student);//adding student to the list
        student = new Student(555234563,"Fang Zhou");//Creating new object of the student
        studentsArts.add(student);//adding student to the list
        
        List<Department> departments = new ArrayList<Department>();//Creating list of departments
        Department department = new Department("SDNE",studentsSDNE);//Creating an object of department
        departments.add(department);//adding department to the list
        
        department = new Department("ARTS",studentsArts);//Creating a new object of department
        departments.add(department);//adding department to the list
        
        Institute institute = new Institute("Sheridan College", departments);//Creating an object of institute
        System.out.println("STUDENTS: ");//Displaying list of students
        for(Student s: studentsSDNE)
        {
            System.out.println("STUDENT ID: " + s.getId() + "\tSTUDENT NAME: " + s.getName());
        }
        for(Student s: studentsArts)//Displaying list of students
        {
            System.out.println("STUDENT ID: " + s.getId() + "\tSTUDENT NAME: " + s.getName());
        }
        System.out.println();
        System.out.println();
        for(Department d: departments)//Displaying list of departments
        {
            System.out.println("DEPARTMENT: " + d.getName());//Displaying list of departments
            d.Display();//Displaying students per department
        }
        
        System.out.println("INSTITUTE: " + institute.getName()+ " " );//Displaying institute
        System.out.println("DEPARTMENTS:");//printing title for department
        institute.Display();//Displaying departments per institute
    }
}
