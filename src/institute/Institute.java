/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

import java.util.List;

/**
 * Class Institute
 * @author Adeel Khilji
 */
public class Institute 
{
    private String name;//Declaring instance variable name
    private List<Department> departments;//Declaring instance variable List<Department>
    
    /**
     * Constructor with two parameters
     * @param name String
     * @param departments List<Department>
     */
    public Institute(String name, List<Department> departments)
    {
        this.name = name;//Assigning name to this.name
        this.departments = departments;//Assigning departments to this.departments
    }
    
    /**
     * getName() - getter method for name
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
    /**
     * getDepartments() - getter method for departments
     * @return List<Department>
     */
    public List<Department> getDepartments()
    {
        return this.departments;
    }
    /**
     * Display() - Prints the list of departments
     */
    public void Display()
    {
        for(Department d: departments)
        {
            System.out.println(d.getName());
        }
    }
    
}
