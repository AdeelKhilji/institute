/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

/**
 * Class Student
 * @author Adeel Khilji
 */
public class Student 
{
    private int Id;//Declaring instance variable of int
    private String name;//Declaring instance variable of String
    
    /**
     * Constructor with two parameters
     * @param Id int
     * @param name String
     */
    public Student(int Id, String name)
    {
        this.Id = Id;//Assigning Id to this.Id
        this.name = name;//Assigning name to this.name
    }
    
    /**
     * getId() - getter method for Id
     * @return int
     */
    public int getId()
    {
        return this.Id;
    }
    
    /**
     * getName() - getter method for name
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
}
