/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

import java.util.List;

/**
 * Class Department
 * @author Adeel Khilji
 */
public class Department 
{
    private String name;//Declaring instance variable name
    private List<Student> students;//Declaring instance variable List<Student>
    
    /**
     * Constructor with two parameters
     * @param name String
     * @param students List<Student>
     */
    public Department(String name, List<Student> students)
    {
        this.name = name;//Assigning name to this.name
        this.students = students;//Assigning students to this.students
    }
    /**
     * getName() - getter method for name
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
    
    /**
     * getStudent() - getter method for students
     * @return List<Student>
     */
    public List<Student> getStudent()
    {
        return students;
    }
    
    /**
     * Display() - Displays lists of students
     */
    public void Display()
    {
        for(Student s: students)
        {
            System.out.println("STUDENT ID: " + s.getId()+ " STUDENT NAME: " + s.getName());
        }
        System.out.println();
    }
}
